from PIL import Image as img
import matplotlib.pyplot as plt
import numpy as np
import requests
from io import BytesIO

#------------------WCZYTUJĘ OBRAZEK BEZPOŚREDNIO Z SIECI--------------------------------

# Zapisujemy link 
link = requests.get("https://media.comicbook.com/2020/08/cyberpunk-2077-1--1233341-1280x0.jpeg")
# Otwieram obraz pobrany bezpośrednio z sieci
obraz = img.open(BytesIO(link.content))
plt.imshow(obraz)
plt.title('Nieprzetworzony Obrazek')
plt.show()

#-------------------ROBIĘ HISTOGRAM------------------------------------------

# Tworzę wektor numpy z obrazka
lista = np.asarray(obraz)
# Tworzę wyzerowany wektor pod histogram
histogramr= np.zeros((256))
histogramg = np.zeros((256))
histogramb = np.zeros((256))

# Dla każdego koloru r g b liczę ile wystąpiło pikseli o danej intensywności
print('proszę chwilę poczekać, ponieważ trwają obliczenia')
for i in lista[:,:,0]:
    for j in i:        
        histogramr[j]+=1
print('czerowny policzony')
for i in lista[:,:,1]:
    for j in i:        
        histogramg[j]+=1
print('zielony policzony')
for i in lista[:,:,2]:
    for j in i:        
        histogramb[j]+=1
print('niebieski policzony')
# Na potrzeby histogramu potrzebuję wektora o 768 komórkach, gdzie komórka 0 ma wartość 0, komórka 1 ma wartość 1 itd aż do 767
wektorx = np.empty((0), int)
for i in range (0,256):
    wektorx = np.append(wektorx, np.array([i]))
# Tworzę histogram

fig, (ax1, ax2, ax3) = plt.subplots(3)
fig.suptitle('Histogram')
ax1.bar(wektorx,histogramr, color = "red")
ax2.bar(wektorx,histogramg, color = "green")
ax3.bar(wektorx,histogramb, color = "blue")
plt.xlabel('Intensywność RGB')
plt.ylabel('Ilość wystąpień')
plt.title('Histogram')
plt.show()

# -------------------------ROBIĘ HISTOGRAM W SKALI SZAROŚCI-----------------------------------------

print('Proszę chwilę poczekać, bo znowu trwają obliczenia')
# Wektor pod wartości histogramu szarości 
szarosc = np.zeros((256))

#predefiniuję wektor aby stworzyć obrazek w skali szarości. Potem będzie to potrzebne do zrobienia filtrów
gray = np.zeros((670,1280))
flatgray = np.zeros(857600)

p1=0
p2=0
p3=0
# Aby wykonać histogram w skali szarości, obliczam średnią arytmetyczną z RGB każdeg piksela obrazu i zliczam liczbę wystąpień danej intensywności
for i in lista:
    for j in i:        
        srednia1=int(j[0])
        srednia2=int(j[1])
        srednia3=int(j[2])
        srednia = round((srednia1+srednia2+srednia3)/3)       
        szarosc[srednia]+=1
        gray[p1,p2]=srednia
        flatgray[p3]=srednia
        p2+=1
        p3+=1
    p2=0
    p1+=1   

# Na potrzeby histogramu szarości potrzebuję wektora o 256 komórkach, gdzie komórka 0 ma wartość 0, komórka 1 ma wartość 1 itd aż do 256
wektorxs = np.empty((0), int)
for i in range (0,256):
   wektorxs = np.append(wektorxs, np.array([i]))
# Tworzę histogram szarość
plt.bar(wektorxs, szarosc,color = "gray")
plt.xlabel('Intensywność szarości')
plt.ylabel('Ilość wystąpień')
plt.title('Histogram w skali Szarości')
plt.show()

# -------------------------ROBIĘ FILTR SOBELA-----------------------------------------

#Wyświetlam sobie obraz w skali szarości
obrazszary = img.fromarray(gray)
plt.imshow(obrazszary)
plt.title('Obraz w skali szarości')
plt.show()

#Definiuję jednowymiarowe macierze Sobela
Sobelx = np.array([1,0,-1,2,0,-2, 1,0,-1])
Sobely = np.array([1,2,1,0,0,0,-1,-2,-1])

# Dokonuję splotu mojego obrazu z macierzami Sobela
xplot = np.convolve(flatgray,Sobelx, mode="same")
yplot = np.convolve(flatgray,Sobely, mode="same")
# Zmieniam tym na int(liczbę całkowitą) aby móc odtworzyć obraz z wektora
xplot= xplot.astype(int)
yplot= yplot.astype(int)
# Liczę gradient
Sobel = np.sqrt(np.add (np.square(xplot),np.square(yplot) ) )
# Upewniam się że wektor ma wymiary 670x1280
Sobel = np.reshape(Sobel,(670,1280))
# Wyświetlam wynik działania filtru Sobela
obrazsobel = img.fromarray(Sobel)
plt.imshow(obrazsobel)
plt.title('Filtr Sobela')
plt.show()

# -------------------------ROBIĘ KRZYŻ ROBERTS'A-----------------------------------------
# Tu w sumie nie ma nic odkrywczego, bo robimy dokładnie to so przed chwilą, tylko mamy inne macierze

#Definiuję jednowymiarowe macierze Robertsa
Robertsx = np.array([1,0,0,-1])
Robertsy = np.array([0,1,-1,0])

# Dokonuję splotu mojego obrazu z macierzami Robertsa
crossplotx = np.convolve(flatgray,Robertsx, mode="same")
crossploty = np.convolve(flatgray,Robertsy, mode="same")
# Zmieniam tym na int(liczbę całkowitą) aby móc odtworzyć obraz z wektora
crossplotx= crossplotx.astype(int)
crossploty= crossploty.astype(int)
# Liczę gradient
Roberts = np.sqrt(np.add (np.square(crossplotx),np.square(crossploty) ) )
# Upewniam się że wektor ma wymiary 670x1280
Roberts = np.reshape(Roberts,(670,1280))
# Wyświetlam wynik działania Krzyża Robertsa
obrazroberts = img.fromarray(Roberts)
plt.imshow(obrazroberts)
plt.title('Krzyż Robertsa')
plt.show()