# README

Ten kod stanowi implementację kilku kluczowych operacji na obrazach cyfrowych, realizowanych za pomocą Pythona. Główne funkcje tego skryptu obejmują:

1. Wczytywanie obrazu z sieci.
2. Generowanie histogramów RGB dla danego obrazu.
3. Generowanie histogramu w skali szarości dla danego obrazu.
4. Implementację i zastosowanie filtrów przetwarzania obrazów - Sobela i Krzyża Robertsa.

Poniżej znajduje się bardziej szczegółowy opis tych funkcji: 

## Wczytywanie obrazu z sieci

Pierwszy segment kodu jest odpowiedzialny za wczytanie obrazu bezpośrednio z sieci. Wykorzystuję do tego bibliotekę `requests` w celu pobrania obrazu, a następnie przetwarzam go za pomocą biblioteki `PIL`, która umożliwia otwarcie i manipulację obrazem.

## Generowanie histogramów RGB

Następny segment kodu generuje histogramy RGB dla danego obrazu. Histogramy RGB dostarczają informacji o rozkładzie intensywności kolorów czerwonego, zielonego i niebieskiego w obrazie. Proces ten jest realizowany poprzez iterację przez piksele obrazu i zliczanie liczby wystąpień poszczególnych intensywności kolorów.

## Generowanie histogramu w skali szarości

Kolejny segment kodu generuje histogram w skali szarości dla danego obrazu. Histogram w skali szarości dostarcza informacji o rozkładzie intensywności pikseli obrazu. Proces ten jest realizowany poprzez obliczanie średniej arytmetycznej z wartości RGB dla każdego piksela i następnie zliczanie liczby wystąpień poszczególnych intensywności.

## Filtry przetwarzania obrazów - Sobel i Krzyż Robertsa

Ostatnie segmenty kodu implementują i stosują dwa różne filtry przetwarzania obrazów: filtr Sobela i Krzyża Robertsa. Filtry te są stosowane do wykrywania krawędzi na obrazie. Filtr Sobela działa na zasadzie konwolucji obrazu z predefiniowanymi macierzami Sobela, podobnie jak Krzyż Robertsa. Oba filtry zwracają obrazy, które pokazują miejsca o wysokim kontraście intensywności, które często odpowiadają krawędziom obiektów na obrazie.
